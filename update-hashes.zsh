#!/usr/bin/env zsh

DIR=${1:-.}
LN=ln

# Support running from macOS with gln
if (( $+commands[gln] )); then
    LN=gln
fi

READELF=readelf

if ! (( $+commands[readelf] )); then
    READELF=$(find /usr/local/Cellar -iname greadelf | head -1)
fi

TOUCH=touch

if (( $+commands[gtouch] )); then
    TOUCH=gtouch
fi

FIND=find

if (( $+commands[gfind] )); then
    FIND=gfind
fi


echo "Using ln      = $LN"
echo "Using readelf = $READELF"
echo "Using touch   = $TOUCH"
echo "Using find    = $FIND"

if [ -z "$(which $READELF)" ]; then
    echo "Must install binutils first for readelf!"
    exit 1
fi


function skip_dev() {
    [[ "$file" =~ "-dev[-_]" ]]
}

$TOUCH -d "10 hours ago" hashes-updated.timestamp

FIND_EXPR=(
    $FIND
    $DIR
    -iname '*libc*.so'
)

if [[ "$DIR" == "." ]]; then
    FIND_EXPR+=( -newer hashes-updated.timestamp )
fi

function link_exists() {
    if [ -e "${1:a}" ]; then
        echo "Link exists already [$1] ${1:a}"
        true
    else
        false
    fi
}

"${FIND_EXPR[@]}" | while read file; do
    echo "Looking at $file"
    added=false

    if ! (file "$file" | grep ELF) &>/dev/null; then
        echo "... not an ELF"
        continue
    fi

#---------------------------------- BUILD ID ----------------------------------
    build_id=$($READELF -n $file 2>&1 | grep "Build ID" | cut -f 2 -d ':')

    if [[ -z "$build_id" ]];
    then
        echo "[BuildID] Skipping $file because it has no build ID"
    else
        build_id=${build_id//[[:blank:]]/}
        
        link=hashes/build_id/$build_id
        if ! link_exists "$link"; then
            printf "%8s %64s %s\n" BuildID "$build_id" "$file"
            $LN -srf $file $link
            added=true
        fi
    fi

#------------------------------------ SHA1 ------------------------------------
    SHA1=$(sha1sum $file | awk '{print $1}')
    link=hashes/sha1/$SHA1
    if ! link_exists "$link"; then
        printf "%8s %64s %s\n" SHA1 "$SHA1" "$file"
        $LN -srf $file $link
        added=true
    fi

#----------------------------------- SHA256 -----------------------------------
    SHA256=$(sha256sum $file | awk '{print $1}')
    link=hashes/sha256/$SHA256
    if ! link_exists "$link"; then
        printf "%8s %64s %s\n" SHA256 "$SHA256" "$file"
        $LN -srf $file $link
        added=true
    fi

#------------------------------------ MD5 -------------------------------------
    MD5=$(md5sum $file | awk '{print $1}')
    link=hashes/md5/$MD5
    if ! link_exists "$link"; then
        printf "%8s %64s %s\n" MD5 "$MD5" "$file"
        $LN -srf $file $link
        added=true
    fi

    if $added; then
        git add "$file"
    fi
done

git add hashes

if [ -n "$(git diff --name-only --cached)" ]; then
    git commit -m "Update hashes $@"
fi