#!/usr/bin/env zsh

zsh harvest-docker.zsh docker/archlinux:base
zsh harvest-docker.zsh docker/archlinux:latest

zsh harvest-docker.zsh docker/ubuntu:focal
zsh harvest-docker.zsh docker/ubuntu:bionic
zsh harvest-docker.zsh docker/ubuntu:xenial
zsh harvest-docker.zsh docker/ubuntu:trusty

docker image ls | awk '{print $3}' | xargs docker rmi
