#!/usr/bin/env zsh
URLS=( "http://rpmfind.net/linux/rpm2html/search.php?query=libc.so.6" )

if ! (( $+commands[rpm] )); then
    >&2 echo RPM is not installed, skipping RPM downloads
    exit 1
fi

function is_marked {
    # If it's a deb-file, don't try to read it
    if file "$1" | grep RPM > /dev/null;
    then
        false
        return
    fi

    # Ensure the commit actually exists
    commit="$(head -c40 $1)"
    if [ "$(git cat-file -t $commit 2>/dev/null)" = "commit" ];
    then
        true
        return
    fi

    false
    return
}

function mark {
    rm -f $1
    echo $2 > $1
}


function download {
    python3 rpm.py
}

function check {
    rpm --import keys

    find $1 -iname '*.rpm' | while read file; do
        if is_marked $file;
        then
            echo "$file OK"
            continue
        else
            rpm --checksig $file # || rm -f $file
        fi
    done
}

function extract {
find $1 -iname '*.rpm' | while read rpm; do
    echo $rpm
    dir="libc/${rpm:t:r}"

    echo "Checking $rpm"
    if is_marked "$rpm" ;
    then
        echo "...skipping"
        continue
    fi

    [[ -d $dir ]] || mkdir -p $dir

    echo "Extracting $rpm"
    rpm2cpio "$rpm" \
    | (cd $dir \
        && cpio \
            -ivd \
            '*/libc-*.so' \
            '*/libc.so*' \
            '*/libc.a')

    echo "Committing $rpm"
    git add libc || continue
    git commit -m "$rpm"

    mark $rpm $(git rev-parse HEAD)
    git add $rpm
    git commit -m "update .rpms"
done
}

case "$1" in
    check)
        check    rpmfiles
        ;;
    download)
        download rpmfiles
        ;;
    extract)
        extract  rpmfiles
        ;;
    *)
        download rpmfiles
        check    rpmfiles
        extract  rpmfiles
        ;;
esac

